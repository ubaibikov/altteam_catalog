  {if $filter.slider}
    {if $filter.feature_type == "ProductFeatures::DATE"|enum}
        {include file="blocks/product_filters/components/product_filter_datepicker.tpl" filter_uid=$filter_uid filter=$filter}
    {else}
        {include file="blocks/product_filters/components/product_filter_slider.tpl" filter_uid=$filter_uid filter=$filter}
    {/if}
    
{else}
    {if $filter.field_type == 'K'}
        {include file="addons/altteam_catalog/blocks/product_filter_catalog.tpl" filter_uid=$filter_uid filter=$filter collapse=$collapse}
    {else} 
        {include file="blocks/product_filters/components/product_filter_variants.tpl" filter_uid=$filter_uid filter=$filter collapse=$collapse}
    {/if}
{/if}