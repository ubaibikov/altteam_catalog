<div class="hidden">
    {foreach $filter.variants as $variant}
        <input class="cm-product-filters-checkbox" type="radio" name="product_filters[{$filter.filter_id}]" data-ca-filter-id="{$filter.filter_id}" value="{$variant.variant_id}" id="elm_checkbox_{$filter_uid}_{$variant.variant_id}" />
    {/foreach}
    {foreach $filter.selected_variants as $variant}
            <input class="cm-product-filters-checkbox" type="radio" name="product_filters[{$filter.filter_id}]" data-ca-filter-id="{$filter.filter_id}" value="{$variant.variant_id}" id="elm_checkbox_{$filter_uid}_{$variant.variant_id}" checked="checked" /> 
    {/foreach}
</div>
<ul class="ty-product-filters {if $collapse}hidden{/if}" id="content_{$filter_uid}">
     <li class="ty-product-filters__item-more">
            <select class="single" id="catalog_{$filter_uid}" >
                <option value="oc" {if $filter.selected_variants.oc}selected="selected"{/if}>{__('only_catalog')}</option>
                <option value="sc" {if $filter.selected_variants.sc}selected="selected"{/if}>{__('stock_and_catalog')}</option>
                <option value="os" {if $filter.selected_variants.os}selected="selected"{/if}>{__('only_in_stock')}</option>
            </select>
        <li class="ty-product-filters__item-more">
</ul>
<script>
    (function(_, $){   
        $.ceEvent('on', 'ce.commoninit', function(context) {
                var filterUid = "{$filter_uid}";
                $("#catalog_{$filter_uid}").change(function(){
                    $('#elm_checkbox_'+filterUid+'_'+$(this).val()).trigger('click');
                });
        });
    }(Tygh, Tygh.$));
</script>