{if !"ULTIMATE:FREE"|fn_allowed_for}
    <div class="control-group" id="altteam_catalog">
    <label class="control-label" for="elm_prodcut_catalog">{__("catalog")}:</label>
        <div class="controls">
            <label class="checkbox">
                <input type="hidden" name="product_data[product_catalog]" value="N" />
                <input type="checkbox" name="product_data[product_catalog]" id="elm_prodcut_catalog" value="Y"{if $product_data.product_catalog == "Y"}checked="checked"{/if} />
            </label>
        </div>
    </div>
{/if}