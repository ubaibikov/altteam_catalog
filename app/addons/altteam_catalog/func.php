<?php

/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
 ****************************************************************************/


use Tygh\Registry;
use Tygh\Enum\ProductTracking;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

function fn_altteam_catalog_get_product_filter_fields(&$filters)
{
    $filters['K'] = array(
        'db_field' => 'product_catalog',
        'table' => 'products',
        'description' => 'catalog',
        'condition_type' => 'S',
    );
}

function fn_altteam_catalog_get_current_filters_post($params, &$filters, $selected_filters, $area, $lang_code, $variant_values, $range_values, &$field_variant_values, $field_range_values)
{
    $fields = fn_get_product_filter_fields();

    foreach ($filters as $filter) {
        $structure = $fields[$filter['field_type']];
        if ($structure['condition_type'] == 'S') {
            $field_variant_values[$filter['filter_id']] = array(
                'variants' => array(
                    'oc' => array(
                        'variant_id' => 'oc'
                    ),
                    'sc' => array(
                        'variant_id' => 'sc'
                    ),
                    'os' => array(
                        'variant_id' => 'os'
                    ),
                )
            );
        }
    }
}

function fn_altteam_catalog_generate_filter_field_params(&$params, $filters, &$selected_filters, &$filter_fields, &$filter, &$structure)
{
    if ($structure['condition_type'] == 'S') {
        $params["change_option"] = current($selected_filters[$filter['filter_id']]);
    }
}

function fn_altteam_catalog_get_products(&$params, $fields, $sortings, &$condition, &$join, $sorting, $group_by, $lang_code, $having)
{
    if ($params['area'] == 'C') {
        if (Registry::get('settings.General.show_out_of_stock_products') == 'N') {
            if (!empty($params['change_option'])) {
                if ($params['change_option'] == 'oc') {
                    $condition .= db_quote(' AND products.product_catalog = ?s', "Y");
                }
                if ($params['change_option'] == 'os') {
                    $condition .= db_quote(' AND products.product_catalog != ?s AND IF(products.tracking = ?s, inventory.amount >= ?i, products.amount >= ?i)', "Y", 0, ProductTracking::TRACK_WITH_OPTIONS, 1, 1);
                }
                if ($params['change_option'] == 'sc') {
                    $condition .= db_quote(' AND (products.product_catalog = ?s OR IF(products.tracking = ?s, inventory.amount >= ?i, products.amount >= ?i))', "Y",  ProductTracking::TRACK_WITH_OPTIONS, 1, 1);
                }
            }
        }
        elseif(Registry::get('settings.General.show_out_of_stock_products') == 'Y'){
            $condition .= db_quote(' AND products.product_catalog != ?s', "Y");
        }
    }
}
